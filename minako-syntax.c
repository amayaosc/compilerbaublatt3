#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "minako.h"

#define DIE(...)                                                               \
  do {                                                                         \
    fflush(stdout);                                                            \
    fprintf(stderr, __VA_ARGS__);                                              \
    fflush(stderr);                                                            \
    exit(-1);                                                                  \
  } while (0)

#define PARSE_ERROR()                                                          \
  DIE("ERROR: %s:%d:%s(): Syntaxfehler in Zeile %d\n",                         \
      __FILE__, __LINE__, __func__, yylineno)

int currentToken;
int nextToken;

void eat() {
  currentToken = nextToken;
  nextToken = yylex();
}

int isToken(int token) {
  return currentToken == token;
}

int isNextToken(int token) {
  return nextToken == token;
}

void isTokenAndEat(int token) {
  if (!isToken(token)) PARSE_ERROR();
  eat();
}

void parse();
void parse_program();
void parse_functiondefinition();
void parse_functioncall();
void parse_statementlist();
void parse_block();
void parse_statement();
void parse_ifstatement();
void parse_returnstatement();
void parse_printf();
void parse_type();
void parse_statassignment();
void parse_assignment();
void parse_expr();
void parse_simpexpr();
void parse_term();
void parse_factor();

void parse() {
  currentToken = yylex();
  nextToken = yylex();
  parse_program();
}

void parse_program() {
  while (isToken(KW_BOOLEAN) || isToken(KW_FLOAT) || isToken(KW_INT)
      || isToken(KW_VOID)) {
    parse_functiondefinition();
  }
  isTokenAndEat(EOF);
}

void parse_functiondefinition() {
  if (isToken(KW_BOOLEAN) || isToken(KW_FLOAT) || isToken(KW_INT)
      || isToken(KW_VOID)) {
    parse_type();
    isTokenAndEat(ID);
    isTokenAndEat('(');
    isTokenAndEat(')');
    isTokenAndEat('{');
    parse_statementlist();
    isTokenAndEat('}');
  }
  else {
    PARSE_ERROR();
  }
}

void parse_functioncall() {
  isTokenAndEat(ID);
  isTokenAndEat('(');
  isTokenAndEat(')');
}

void parse_statementlist() {
  while (isToken('{') || isToken(ID) || isToken(KW_IF) || isToken(KW_RETURN)
      || isToken(KW_PRINTF)) {
    parse_block();
  }
}

void parse_block() {
  if (isToken('{')) {
    eat();
    parse_statementlist();
    isTokenAndEat('}');
  }
  else if (isToken(KW_IF) || isToken(KW_RETURN) || isToken(KW_PRINTF)
      || isToken(ID)) {
    parse_statement();
  }
  else {
    PARSE_ERROR();
  }
}

void parse_statement() {
  if (isToken(KW_IF)) {
    parse_ifstatement();
  }
  else if (isToken(KW_RETURN)) {
    parse_returnstatement();
    isTokenAndEat(';');
  }
  else if (isToken(KW_PRINTF)) {
    parse_printf();
    isTokenAndEat(';');
  }
  else if (isToken(ID) && isNextToken('=')) {
    parse_statassignment();
    isTokenAndEat(';');
  }
  else if (isToken(ID) && isNextToken('(')) {
    parse_functioncall();
    isTokenAndEat(';');
  }
  else {
    PARSE_ERROR();
  }
}

void parse_ifstatement() {
  isTokenAndEat(KW_IF);
  isTokenAndEat('(');
  parse_assignment();
  isTokenAndEat(')');
  parse_block();
}

void parse_returnstatement() {
  isTokenAndEat(KW_RETURN);

  if (!isToken(';')) {
    parse_assignment();
  }
}

void parse_printf() {
  isTokenAndEat(KW_PRINTF);
  isTokenAndEat('(');
  parse_assignment();
  isTokenAndEat(')');
}

void parse_type() {
  if (isToken(KW_BOOLEAN) || isToken(KW_FLOAT)
        || isToken(KW_INT) || isToken(KW_VOID)) {
    eat();
  }
  else {
    PARSE_ERROR();
  }
}

void parse_statassignment() {
  isTokenAndEat(ID);
  isTokenAndEat('=');
  parse_assignment();
}

void parse_assignment() {
  if (isToken(ID) && isNextToken('=')) {
    eat();
    isTokenAndEat('=');
    parse_assignment();
  }
  else if (isToken('-') || isToken(CONST_INT) || isToken(CONST_FLOAT)
        || isToken(CONST_BOOLEAN) || isToken(ID) || isToken('(')) {
    parse_expr();
  }
  else {
    PARSE_ERROR();
  }
}

void parse_expr() {
  parse_simpexpr();
  if (isToken(EQ) || isToken(NEQ) || isToken(LEQ) || isToken(GEQ) || isToken(LSS)
      || isToken(GRT)) {
    eat();
    parse_simpexpr();
  }
}

void parse_simpexpr() {
  if (isToken('-')) {
    eat();
  }
  parse_term();
  while (isToken('+') || isToken('-') || isToken(OR)) {
    eat();
    parse_term();
  }
}

void parse_term() {
  parse_factor();
  while (isToken('*') || isToken('/') || isToken(AND)) {
    eat();
    parse_factor();
  }
}

void parse_factor() {
  if (isToken(ID) && isNextToken('(')) {
    parse_functioncall();
  }
  else if (isToken(CONST_INT) || isToken(CONST_FLOAT) || isToken(CONST_BOOLEAN)
      || isToken(ID)) {
    eat();
  }
  else if (isToken('(')) {
    eat();
    parse_assignment();
    isTokenAndEat(')');
  }
  else {
    PARSE_ERROR();
  }
}

int main(int argc, char** argv) {
  yyin = argc > 1 ? fopen(argv[1], "r") : stdin;
  if (!yyin) DIE("Error: Invalid input file.\n");
  parse();
  fclose(yyin);
}
